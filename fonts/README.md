Scalable Screen Font 2.0 - Example Fonts
========================================

 - Free*.sfn - vector based font with cubic Bezier curves, converted from [GNU FreeFont](https://www.gnu.org/software/freefont/)'s OTF files

 - Vera*.sfn - vector based fonts with quadratic Bezier curves, converted from [GNOME Bitstream Vera Font](https://www.gnome.org/fonts/)'s TTF files

 - Vera.sfn - an SSFN font collection of the above fonts

 - u_vga16.sfn.gz - bitmap font, converted from [u_vga16.bdf](http://www.inp.nsk.su/~bolkhov/files/fonts/univga/), made by Dmitry Bolkhovityanov

 - unifont.sfn.gz bitmap font, converted from [unifont.hex](http://unifoundry.com/unifont/index.html) of the GNU unifont project

 - chrome.sfn - pixmap font, Retro Chrome looking, made after an image found by Google

 - emoji.sfn - pixmap font, created from a screenshot of unicode.org's emoji page

 - stoneage.sfn - pixmap font from an old DOS game, which I rewrote for Linux, [xstoneage](https://gitlab.com/bztsrc/xstoneage)

 - bende.sfn - my little boy's secret writing system, entirely made with sfnedit
